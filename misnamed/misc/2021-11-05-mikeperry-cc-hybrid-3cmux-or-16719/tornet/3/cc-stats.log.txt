
Network statistics for circuits >= 4MB xmit:
  Stats for phase post-4mb:
    BWE: Min: 1.0/934.6+603.7/5792.0, Avg: 1.0/1122.1+605.5/5868.6, Dev: 102.4, Max: 1.0/1302.3+641.7/6165.0
    CWND: Min: 109.0/581.3+197.5/3609.0, Avg: 168.3/640.7+196.0/3615.0, Dev: 38.6, Max: 197.0/713.4+205.5/3642.0
    QUSE: Min: 4.0/71.0+48.9/937.0, Avg: 5.5/107.9+70.4/1138.6, Dev: 28.9, Max: 6.0/177.1+141.9/1783.0
    final CWND: 155.0/700.8+211.1/3642.0
    final QUSE: 6.0/116.9+86.9/1642.0
    RTTFactor: Min: 1.0/1.2+2.0/436.1, Avg: 1.0/1.7+4.1/612.7, Dev: 0.7, Max: 1.0/4.4+24.2/1622.8
    init minRTTFactor: 1.0/1.0+0.1/6.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 132132
  Stats for phase slow start:
    BWE: Min: 1.0/111.0+49.3/513.0, Avg: 1.0/272.1+175.1/2626.8, Dev: 147.5, Max: 1.0/524.6+475.2/8643.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/148.0+67.1/928.8, Dev: 80.7, Max: 60.0/287.3+216.6/3444.0
    QUSE: Min: 1.0/1.9+2.3/59.0, Avg: 1.0/10.0+13.0/133.3, Dev: 8.1, Max: 1.0/24.0+37.2/269.0
    final CWND: 60.0/287.3+216.6/3444.0
    final QUSE: 1.0/38.4+65.1/1439.0
    RTTFactor: Min: 1.0/1.0+0.1/13.1, Avg: 1.0/1.1+0.9/91.7, Dev: 0.1, Max: 1.0/1.3+2.6/298.8
    init minRTTFactor: 1.0/1.2+4.1/979.7
    phase minRTTFactor: 1.0/1.1+0.9/226.5
    Total circs: 132132
  Stats for phase post-slow start:
    BWE: Min: 1.0/779.6+536.4/5557.0, Avg: 8.1/1057.5+569.5/6274.3, Dev: 142.1, Max: 13.0/1335.1+644.2/8070.0
    CWND: Min: 109.0/497.2+187.5/3589.0, Avg: 191.6/600.0+189.0/3612.0, Dev: 62.5, Max: 270.0/723.7+201.7/3642.0
    QUSE: Min: 1.0/27.7+31.3/490.0, Avg: 5.4/95.2+63.9/844.5, Dev: 38.4, Max: 7.0/198.2+156.5/1783.0
    final CWND: 155.0/700.8+211.1/3642.0
    final QUSE: 6.0/116.9+86.9/1642.0
    RTTFactor: Min: 1.0/1.1+0.3/25.5, Avg: 1.0/1.6+2.3/172.9, Dev: 0.8, Max: 1.0/5.3+27.6/1622.8
    init minRTTFactor: 1.0/1.0+0.2/59.4
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 132132
  Stats for phase lifetime:
    BWE: Min: 1.0/108.5+51.6/513.0, Avg: 7.9/981.1+519.3/5489.8, Dev: 283.5, Max: 15.0/1336.8+644.6/8643.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 189.5/554.9+159.4/2934.1, Dev: 151.3, Max: 272.0/725.3+201.4/3642.0
    QUSE: Min: 1.0/1.9+2.3/59.0, Avg: 5.1/87.1+57.5/655.2, Dev: 45.5, Max: 7.0/198.2+156.5/1783.0
    final CWND: 155.0/700.8+211.1/3642.0
    final QUSE: 6.0/116.9+86.9/1642.0
    RTTFactor: Min: 1.0/1.0+0.0/4.2, Avg: 1.0/1.6+2.2/160.2, Dev: 0.8, Max: 1.0/5.4+27.6/1622.8
    init minRTTFactor: 1.0/1.2+4.1/979.7
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 132132
  Total circs: 132132
  Circs still in SS: 0

Network statistics for all circuits:
  Stats for phase post-4mb:
    BWE: Min: 1.0/934.6+603.7/5792.0, Avg: 1.0/1122.1+605.5/5868.6, Dev: 102.4, Max: 1.0/1302.3+641.7/6165.0
    CWND: Min: 109.0/581.3+197.5/3609.0, Avg: 168.3/640.7+196.0/3615.0, Dev: 38.6, Max: 197.0/713.4+205.5/3642.0
    QUSE: Min: 4.0/71.0+48.9/937.0, Avg: 5.5/107.9+70.4/1138.6, Dev: 28.9, Max: 6.0/177.1+141.9/1783.0
    final CWND: 155.0/700.8+211.1/3642.0
    final QUSE: 6.0/116.9+86.9/1642.0
    RTTFactor: Min: 1.0/1.2+2.0/436.1, Avg: 1.0/1.7+4.1/612.7, Dev: 0.7, Max: 1.0/4.4+24.2/1622.8
    init minRTTFactor: 1.0/1.0+0.1/6.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 132132
  Stats for phase slow start:
    BWE: Min: 1.0/115.8+51.0/578.0, Avg: 1.0/291.2+206.0/2948.9, Dev: 163.3, Max: 1.0/580.2+585.7/9799.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/152.0+82.9/1281.9, Dev: 85.8, Max: 60.0/305.0+270.4/5166.0
    QUSE: Min: 1.0/2.0+2.6/60.0, Avg: 1.0/10.5+13.5/137.7, Dev: 8.6, Max: 1.0/25.6+38.8/269.0
    final CWND: 60.0/305.0+270.4/5166.0
    final QUSE: 0.0/37.5+63.5/1993.0
    RTTFactor: Min: 1.0/1.0+2.8/2064.2, Avg: 1.0/1.2+4.8/2064.2, Dev: 0.2, Max: 1.0/1.4+8.7/3556.5
    init minRTTFactor: 1.0/1.2+2.5/979.7
    phase minRTTFactor: 1.0/1.0+0.6/226.5
    Total circs: 557994
  Stats for phase post-slow start:
    BWE: Min: 1.0/921.4+617.9/8455.0, Avg: 1.0/1096.1+622.9/8927.1, Dev: 96.7, Max: 1.0/1264.7+662.1/9366.0
    CWND: Min: 109.0/529.0+223.0/3611.0, Avg: 191.6/589.1+220.3/3621.0, Dev: 38.2, Max: 270.0/658.9+228.0/3649.0
    QUSE: Min: 1.0/34.8+46.5/1469.0, Avg: 1.0/84.7+69.2/1469.0, Dev: 30.5, Max: 1.0/149.2+136.4/1993.0
    final CWND: 155.0/648.1+228.6/3649.0
    final QUSE: 1.0/103.6+88.2/1715.0
    RTTFactor: Min: 1.0/1.2+4.0/1372.6, Avg: 1.0/1.6+6.5/2251.9, Dev: 0.5, Max: 1.0/3.5+22.8/3844.6
    init minRTTFactor: 1.0/1.0+0.2/59.4
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 421456
  Stats for phase lifetime:
    BWE: Min: 1.0/114.9+51.9/578.0, Avg: 1.0/722.6+506.4/6045.2, Dev: 304.9, Max: 1.0/1090.0+730.1/9799.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/386.6+204.8/2934.1, Dev: 159.4, Max: 60.0/569.2+300.1/5166.0
    QUSE: Min: 1.0/2.0+2.5/60.0, Avg: 1.0/51.9+52.6/655.2, Dev: 33.2, Max: 1.0/118.7+131.7/1993.0
    final CWND: 60.0/559.4+297.2/5166.0
    final QUSE: 0.0/83.7+86.1/1715.0
    RTTFactor: Min: 1.0/1.0+2.8/2064.2, Avg: 1.0/1.4+5.6/2064.2, Dev: 0.5, Max: 1.0/3.1+21.5/3844.6
    init minRTTFactor: 1.0/1.2+2.5/979.7
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 557994
  Total circs: 557994
  Circs still in SS: 136538

Network statistics for circuits never exiting slow start:
  Stats for phase post-4mb:
    No circuits
  Stats for phase slow start:
    BWE: Min: 1.0/114.9+50.6/578.0, Avg: 1.0/269.2+221.5/2472.2, Dev: 146.8, Max: 1.0/537.3+647.5/8333.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/141.9+98.1/1281.9, Dev: 78.1, Max: 60.0/285.6+317.1/5166.0
    QUSE: Min: 1.0/2.1+3.0/60.0, Avg: 1.0/9.5+12.9/132.4, Dev: 7.6, Max: 1.0/23.1+37.3/269.0
    final CWND: 60.0/285.6+317.1/5166.0
    final QUSE: 0.0/22.2+36.3/269.0
    RTTFactor: Min: 1.0/1.1+5.7/2064.2, Avg: 1.0/1.3+9.2/2064.2, Dev: 0.2, Max: 1.0/1.5+15.6/3556.5
    init minRTTFactor: 1.0/1.1+1.3/237.7
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 136538
  Stats for phase post-slow start:
    No circuits
  Stats for phase lifetime:
    BWE: Min: 1.0/114.9+50.6/578.0, Avg: 1.0/269.2+221.5/2472.2, Dev: 146.8, Max: 1.0/537.3+647.5/8333.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/141.9+98.1/1281.9, Dev: 78.1, Max: 60.0/285.6+317.1/5166.0
    QUSE: Min: 1.0/2.1+3.0/60.0, Avg: 1.0/9.5+12.9/132.4, Dev: 7.6, Max: 1.0/23.1+37.3/269.0
    final CWND: 60.0/285.6+317.1/5166.0
    final QUSE: 0.0/22.2+36.3/269.0
    RTTFactor: Min: 1.0/1.1+5.7/2064.2, Avg: 1.0/1.3+9.2/2064.2, Dev: 0.2, Max: 1.0/1.5+15.6/3556.5
    init minRTTFactor: 1.0/1.1+1.3/237.7
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 136538
  Total circs: 136538
  Circs still in SS: 136538

Network statistics for circuits that hit min cwnd:
  No circuits
TLS overhead in /shadow/hybrid-3cmux-or/16719/49984/tornet/3/heartbeat-nonexit.log min/avg+dev/max: 0.78/1.69+1.4/7.63
TLS overhead in /shadow/hybrid-3cmux-or/16719/49984/tornet/3/heartbeat-exit.log min/avg+dev/max: 0.73/2.70+4.7/24.30
