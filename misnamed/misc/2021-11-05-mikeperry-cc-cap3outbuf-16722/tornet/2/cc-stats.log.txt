
Network statistics for circuits >= 4MB xmit:
  Stats for phase post-4mb:
    BWE: Min: 1.0/928.3+584.4/5391.0, Avg: 1.0/1112.7+588.5/5559.8, Dev: 100.7, Max: 1.0/1290.5+626.1/6077.0
    CWND: Min: 109.0/583.3+195.9/3309.0, Avg: 149.7/642.9+194.6/3354.5, Dev: 38.8, Max: 189.0/716.1+204.5/3409.0
    QUSE: Min: 3.0/70.7+49.3/1538.0, Avg: 6.4/106.4+70.2/1589.0, Dev: 28.0, Max: 9.0/174.3+140.8/2450.0
    final CWND: 154.0/703.9+209.9/3309.0
    final QUSE: 7.0/116.1+87.7/1671.0
    RTTFactor: Min: 1.0/1.2+1.8/300.2, Avg: 1.0/1.7+3.4/367.8, Dev: 0.5, Max: 1.0/3.4+15.4/1115.7
    init minRTTFactor: 1.0/1.0+0.1/14.6
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 132638
  Stats for phase slow start:
    BWE: Min: 1.0/110.4+48.3/653.0, Avg: 1.0/270.0+170.4/2413.1, Dev: 146.0, Max: 1.0/519.4+460.4/7411.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/147.9+66.5/928.8, Dev: 80.5, Max: 60.0/286.6+214.5/3444.0
    QUSE: Min: 1.0/1.8+2.1/55.0, Avg: 1.0/9.7+12.5/134.0, Dev: 7.8, Max: 1.0/23.1+36.0/269.0
    final CWND: 60.0/286.6+214.5/3444.0
    final QUSE: 1.0/37.0+63.2/1902.0
    RTTFactor: Min: 1.0/1.0+0.3/101.4, Avg: 1.0/1.1+0.9/164.8, Dev: 0.1, Max: 1.0/1.3+2.7/492.2
    init minRTTFactor: 1.0/1.2+2.2/388.8
    phase minRTTFactor: 1.0/1.0+0.3/78.7
    Total circs: 132637
  Stats for phase post-slow start:
    BWE: Min: 1.0/776.6+519.6/5359.0, Avg: 6.1/1049.3+553.8/6105.5, Dev: 139.3, Max: 11.0/1322.4+625.9/7341.0
    CWND: Min: 107.0/499.5+186.4/3309.0, Avg: 185.7/601.9+187.9/3399.0, Dev: 62.5, Max: 275.0/725.8+200.5/3498.0
    QUSE: Min: 1.0/27.4+31.2/503.0, Avg: 5.7/93.6+63.5/925.6, Dev: 37.4, Max: 9.0/195.1+155.1/2450.0
    final CWND: 154.0/703.9+209.9/3309.0
    final QUSE: 7.0/116.1+87.7/1671.0
    RTTFactor: Min: 1.0/1.1+0.3/26.0, Avg: 1.0/1.5+1.8/107.1, Dev: 0.6, Max: 1.0/4.0+17.6/1115.7
    init minRTTFactor: 1.0/1.0+0.2/20.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 132638
  Stats for phase lifetime:
    BWE: Min: 1.0/108.1+50.2/653.0, Avg: 6.9/972.6+504.0/5429.6, Dev: 281.1, Max: 13.0/1324.2+626.2/7411.0
    CWND: Min: 60.0/60.0+0.7/308.0, Avg: 184.0/556.2+158.1/2855.3, Dev: 152.2, Max: 275.0/727.3+200.3/3498.0
    QUSE: Min: 1.0/1.8+2.1/55.0, Avg: 5.6/85.5+57.1/746.7, Dev: 44.5, Max: 9.0/195.1+155.1/2450.0
    final CWND: 154.0/703.9+209.9/3309.0
    final QUSE: 7.0/116.1+87.7/1671.0
    RTTFactor: Min: 1.0/1.0+0.0/7.2, Avg: 1.0/1.5+1.7/94.8, Dev: 0.6, Max: 1.0/4.1+17.7/1115.7
    init minRTTFactor: 1.0/1.2+2.2/388.8
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 132638
  Total circs: 132638
  Circs still in SS: 0

Network statistics for all circuits:
  Stats for phase post-4mb:
    BWE: Min: 1.0/928.3+584.4/5391.0, Avg: 1.0/1112.7+588.5/5559.8, Dev: 100.7, Max: 1.0/1290.5+626.1/6077.0
    CWND: Min: 109.0/583.3+195.9/3309.0, Avg: 149.7/642.9+194.6/3354.5, Dev: 38.8, Max: 189.0/716.1+204.5/3409.0
    QUSE: Min: 3.0/70.7+49.3/1538.0, Avg: 6.4/106.4+70.2/1589.0, Dev: 28.0, Max: 9.0/174.3+140.8/2450.0
    final CWND: 154.0/703.9+209.9/3309.0
    final QUSE: 7.0/116.1+87.7/1671.0
    RTTFactor: Min: 1.0/1.2+1.8/300.2, Avg: 1.0/1.7+3.4/367.8, Dev: 0.5, Max: 1.0/3.4+15.4/1115.7
    init minRTTFactor: 1.0/1.0+0.1/14.6
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 132638
  Stats for phase slow start:
    BWE: Min: 1.0/114.2+50.2/667.0, Avg: 1.0/286.5+202.0/2985.4, Dev: 160.4, Max: 1.0/569.8+573.0/9327.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/151.6+82.3/1281.9, Dev: 85.5, Max: 60.0/303.8+268.7/5166.0
    QUSE: Min: 1.0/1.9+2.5/60.0, Avg: 1.0/10.3+13.4/147.6, Dev: 8.5, Max: 1.0/25.2+38.6/269.0
    final CWND: 60.0/303.8+268.7/5166.0
    final QUSE: 0.0/36.9+62.8/1902.0
    RTTFactor: Min: 1.0/1.0+0.5/169.8, Avg: 1.0/1.2+1.7/303.0, Dev: 0.1, Max: 1.0/1.4+4.4/563.4
    init minRTTFactor: 1.0/1.1+1.5/500.3
    phase minRTTFactor: 1.0/1.0+0.2/80.0
    Total circs: 556758
  Stats for phase post-slow start:
    BWE: Min: 1.0/909.3+604.4/8656.0, Avg: 1.0/1079.8+611.5/8796.2, Dev: 94.4, Max: 1.0/1244.9+650.6/8925.0
    CWND: Min: 107.0/529.1+221.7/3570.0, Avg: 185.7/589.2+219.3/3580.0, Dev: 38.3, Max: 270.0/659.1+227.4/3592.0
    QUSE: Min: 1.0/34.5+47.0/1138.0, Avg: 1.0/83.5+69.1/1376.7, Dev: 29.7, Max: 1.0/146.7+134.8/2450.0
    final CWND: 154.0/648.7+228.1/3592.0
    final QUSE: 1.0/102.5+88.2/1772.0
    RTTFactor: Min: 1.0/1.2+1.6/393.0, Avg: 1.0/1.5+3.1/437.2, Dev: 0.4, Max: 1.0/2.9+13.3/1374.5
    init minRTTFactor: 1.0/1.0+0.1/20.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 420310
  Stats for phase lifetime:
    BWE: Min: 1.0/113.4+50.9/667.0, Avg: 1.0/711.4+497.2/5429.6, Dev: 300.1, Max: 1.0/1072.4+717.6/9327.0
    CWND: Min: 60.0/60.0+0.3/308.0, Avg: 60.0/386.3+204.8/2855.3, Dev: 159.4, Max: 60.0/568.8+299.9/5166.0
    QUSE: Min: 1.0/1.9+2.5/60.0, Avg: 1.0/51.0+52.2/900.1, Dev: 32.6, Max: 1.0/116.6+130.1/2450.0
    final CWND: 60.0/559.3+297.2/5166.0
    final QUSE: 0.0/82.7+86.0/1772.0
    RTTFactor: Min: 1.0/1.0+0.5/169.8, Avg: 1.0/1.4+2.2/303.0, Dev: 0.4, Max: 1.0/2.6+12.2/1374.5
    init minRTTFactor: 1.0/1.1+1.5/500.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 556759
  Total circs: 556759
  Circs still in SS: 136449

Network statistics for circuits never exiting slow start:
  Stats for phase post-4mb:
    No circuits
  Stats for phase slow start:
    BWE: Min: 1.0/112.8+50.0/475.0, Avg: 1.0/264.2+217.3/2770.4, Dev: 143.8, Max: 1.0/526.3+634.2/9326.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/141.5+97.6/1281.9, Dev: 77.7, Max: 60.0/284.2+316.1/5166.0
    QUSE: Min: 1.0/2.1+2.9/58.0, Avg: 1.0/9.4+12.8/140.8, Dev: 7.5, Max: 1.0/22.8+37.1/269.0
    final CWND: 60.0/284.2+316.1/5166.0
    final QUSE: 0.0/21.9+36.2/269.0
    RTTFactor: Min: 1.0/1.0+0.9/169.8, Avg: 1.0/1.2+2.2/303.0, Dev: 0.2, Max: 1.0/1.4+5.1/469.8
    init minRTTFactor: 1.0/1.1+0.8/192.7
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 136449
  Stats for phase post-slow start:
    No circuits
  Stats for phase lifetime:
    BWE: Min: 1.0/112.8+50.0/475.0, Avg: 1.0/264.2+217.3/2770.4, Dev: 143.8, Max: 1.0/526.3+634.2/9326.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/141.5+97.6/1281.9, Dev: 77.7, Max: 60.0/284.2+316.1/5166.0
    QUSE: Min: 1.0/2.1+2.9/58.0, Avg: 1.0/9.4+12.8/140.8, Dev: 7.5, Max: 1.0/22.8+37.1/269.0
    final CWND: 60.0/284.2+316.1/5166.0
    final QUSE: 0.0/21.9+36.2/269.0
    RTTFactor: Min: 1.0/1.0+0.9/169.8, Avg: 1.0/1.2+2.2/303.0, Dev: 0.2, Max: 1.0/1.4+5.1/469.8
    init minRTTFactor: 1.0/1.1+0.8/192.7
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 136449
  Total circs: 136449
  Circs still in SS: 136449

Network statistics for circuits that hit min cwnd:
  No circuits
TLS overhead in /shadow/cap3outbuf/16722/49995/tornet/2/heartbeat-nonexit.log min/avg+dev/max: 0.79/1.72+1.5/8.05
TLS overhead in /shadow/cap3outbuf/16722/49995/tornet/2/heartbeat-exit.log min/avg+dev/max: 0.73/2.71+4.8/23.63
