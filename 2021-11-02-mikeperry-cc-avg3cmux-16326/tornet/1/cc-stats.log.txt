
Network statistics for circuits >= 4MB xmit:
  Stats for phase post-4mb:
    BWE: Min: 1.0/909.0+600.0/5800.0, Avg: 1.5/1101.1+604.1/5910.6, Dev: 104.0, Max: 2.0/1283.0+642.9/6094.0
    CWND: Min: 83.0/573.5+194.4/3454.0, Avg: 153.4/632.3+193.6/3464.0, Dev: 38.1, Max: 195.0/703.9+204.1/3474.0
    QUSE: Min: 5.0/73.7+51.5/1432.0, Avg: 8.9/112.8+72.5/1726.1, Dev: 30.4, Max: 10.0/185.3+142.9/2320.0
    final CWND: 128.0/689.8+210.8/3454.0
    final QUSE: 9.0/122.8+89.8/1854.0
    RTTFactor: Min: 1.0/1.3+1.6/437.1, Avg: 1.0/1.8+3.5/437.1, Dev: 0.7, Max: 1.0/4.2+20.4/1429.5
    init minRTTFactor: 1.0/1.0+0.1/7.4
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 133643
  Stats for phase slow start:
    BWE: Min: 1.0/110.8+50.3/446.0, Avg: 1.0/270.4+172.0/2306.8, Dev: 145.9, Max: 1.0/519.4+459.3/7282.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/147.7+65.8/928.8, Dev: 80.3, Max: 60.0/285.9+211.1/3444.0
    QUSE: Min: 1.0/1.9+2.3/52.0, Avg: 1.0/10.5+13.2/126.3, Dev: 8.6, Max: 1.0/25.2+38.2/269.0
    final CWND: 60.0/285.9+211.1/3444.0
    final QUSE: 1.0/40.4+67.1/1285.0
    RTTFactor: Min: 1.0/1.0+0.1/23.7, Avg: 1.0/1.1+0.8/105.1, Dev: 0.1, Max: 1.0/1.3+2.4/366.4
    init minRTTFactor: 1.0/1.2+2.4/643.3
    phase minRTTFactor: 1.0/1.0+0.2/36.5
    Total circs: 133643
  Stats for phase post-slow start:
    BWE: Min: 1.0/757.9+530.6/5800.0, Avg: 5.6/1040.8+566.0/6261.7, Dev: 143.2, Max: 13.0/1319.5+640.8/7504.0
    CWND: Min: 83.0/492.1+183.0/3454.0, Avg: 176.7/593.6+185.2/3516.5, Dev: 61.7, Max: 271.0/715.3+198.9/3574.0
    QUSE: Min: 1.0/29.0+32.3/379.0, Avg: 6.9/99.9+66.2/993.2, Dev: 40.3, Max: 10.0/207.5+157.1/2320.0
    final CWND: 128.0/689.8+210.8/3454.0
    final QUSE: 9.0/122.8+89.8/1854.0
    RTTFactor: Min: 1.0/1.1+0.3/13.4, Avg: 1.0/1.6+2.2/93.2, Dev: 0.7, Max: 1.0/5.0+23.7/1429.5
    init minRTTFactor: 1.0/1.0+0.2/16.5
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 133643
  Stats for phase lifetime:
    BWE: Min: 1.0/108.3+52.4/446.0, Avg: 5.5/965.9+516.4/5535.3, Dev: 280.5, Max: 13.0/1321.5+640.9/7504.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 175.2/549.2+156.3/2828.9, Dev: 149.1, Max: 273.0/717.1+198.5/3574.0
    QUSE: Min: 1.0/1.9+2.3/52.0, Avg: 6.5/91.3+59.4/754.3, Dev: 47.7, Max: 10.0/207.5+157.1/2320.0
    final CWND: 128.0/689.8+210.8/3454.0
    final QUSE: 9.0/122.8+89.8/1854.0
    RTTFactor: Min: 1.0/1.0+0.0/4.8, Avg: 1.0/1.6+2.0/88.0, Dev: 0.7, Max: 1.0/5.0+23.8/1429.5
    init minRTTFactor: 1.0/1.2+2.4/643.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 133643
  Total circs: 133643
  Circs still in SS: 0

Network statistics for all circuits:
  Stats for phase post-4mb:
    BWE: Min: 1.0/909.0+600.0/5800.0, Avg: 1.5/1101.1+604.1/5910.6, Dev: 104.0, Max: 2.0/1283.0+642.9/6094.0
    CWND: Min: 83.0/573.5+194.4/3454.0, Avg: 153.4/632.3+193.6/3464.0, Dev: 38.1, Max: 195.0/703.9+204.1/3474.0
    QUSE: Min: 5.0/73.7+51.5/1432.0, Avg: 8.9/112.8+72.5/1726.1, Dev: 30.4, Max: 10.0/185.3+142.9/2320.0
    final CWND: 128.0/689.8+210.8/3454.0
    final QUSE: 9.0/122.8+89.8/1854.0
    RTTFactor: Min: 1.0/1.3+1.6/437.1, Avg: 1.0/1.8+3.5/437.1, Dev: 0.7, Max: 1.0/4.2+20.4/1429.5
    init minRTTFactor: 1.0/1.0+0.1/7.4
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 133643
  Stats for phase slow start:
    BWE: Min: 1.0/115.3+51.7/476.0, Avg: 1.0/288.6+204.5/2858.1, Dev: 161.3, Max: 1.0/573.4+577.7/8953.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/151.5+82.0/1281.9, Dev: 85.3, Max: 60.0/303.3+266.7/5166.0
    QUSE: Min: 1.0/2.0+2.5/60.0, Avg: 1.0/11.0+13.8/142.4, Dev: 9.1, Max: 1.0/26.9+40.2/269.0
    final CWND: 60.0/303.3+266.7/5166.0
    final QUSE: 0.0/39.6+66.2/1371.0
    RTTFactor: Min: 1.0/1.0+0.3/89.2, Avg: 1.0/1.2+1.3/168.1, Dev: 0.1, Max: 1.0/1.4+4.0/834.8
    init minRTTFactor: 1.0/1.2+2.1/643.3
    phase minRTTFactor: 1.0/1.0+0.6/239.0
    Total circs: 557825
  Stats for phase post-slow start:
    BWE: Min: 1.0/898.4+615.9/8713.0, Avg: 1.0/1077.3+622.5/8717.5, Dev: 98.4, Max: 1.0/1248.0+662.8/8722.0
    CWND: Min: 83.0/523.2+218.3/3554.0, Avg: 176.7/582.8+216.1/3564.0, Dev: 37.8, Max: 270.0/651.7+224.3/3574.0
    QUSE: Min: 1.0/36.5+48.4/1214.0, Avg: 1.0/89.2+72.0/1214.0, Dev: 32.2, Max: 1.0/157.1+138.9/2320.0
    final CWND: 126.0/639.5+225.7/3574.0
    final QUSE: 1.0/108.9+91.5/2186.0
    RTTFactor: Min: 1.0/1.2+1.3/390.9, Avg: 1.0/1.6+3.3/438.5, Dev: 0.5, Max: 1.0/3.3+17.1/1447.3
    init minRTTFactor: 1.0/1.0+0.4/181.6
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 422686
  Stats for phase lifetime:
    BWE: Min: 1.0/114.3+52.6/476.0, Avg: 1.0/712.9+503.6/5556.9, Dev: 301.0, Max: 1.0/1078.0+726.8/8953.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/384.1+201.9/2828.9, Dev: 157.8, Max: 60.0/564.7+295.8/5166.0
    QUSE: Min: 1.0/2.0+2.5/60.0, Avg: 1.0/54.7+54.9/754.3, Dev: 35.0, Max: 1.0/125.1+135.1/2320.0
    final CWND: 60.0/553.6+293.0/5166.0
    final QUSE: 0.0/88.1+89.6/2186.0
    RTTFactor: Min: 1.0/1.0+0.2/68.6, Avg: 1.0/1.4+2.1/241.4, Dev: 0.4, Max: 1.0/2.9+15.3/1447.3
    init minRTTFactor: 1.0/1.2+2.1/643.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 557825
  Total circs: 557825
  Circs still in SS: 135139

Network statistics for circuits never exiting slow start:
  Stats for phase post-4mb:
    No circuits
  Stats for phase slow start:
    BWE: Min: 1.0/114.5+51.0/476.0, Avg: 1.0/267.4+219.8/2483.2, Dev: 145.3, Max: 1.0/532.4+641.9/8026.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/141.7+97.7/1281.9, Dev: 77.9, Max: 60.0/285.0+316.0/5166.0
    QUSE: Min: 1.0/2.1+2.8/59.0, Avg: 1.0/9.7+12.9/133.4, Dev: 7.8, Max: 1.0/23.7+38.0/269.0
    final CWND: 60.0/285.0+316.0/5166.0
    final QUSE: 0.0/22.8+37.0/269.0
    RTTFactor: Min: 1.0/1.0+0.5/68.6, Avg: 1.0/1.2+1.5/152.2, Dev: 0.1, Max: 1.0/1.3+4.0/658.8
    init minRTTFactor: 1.0/1.1+1.3/226.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 135139
  Stats for phase post-slow start:
    No circuits
  Stats for phase lifetime:
    BWE: Min: 1.0/114.5+51.0/476.0, Avg: 1.0/267.4+219.8/2483.2, Dev: 145.3, Max: 1.0/532.4+641.9/8026.0
    CWND: Min: 60.0/60.0+0.0/60.0, Avg: 60.0/141.7+97.7/1281.9, Dev: 77.9, Max: 60.0/285.0+316.0/5166.0
    QUSE: Min: 1.0/2.1+2.8/59.0, Avg: 1.0/9.7+12.9/133.4, Dev: 7.8, Max: 1.0/23.7+38.0/269.0
    final CWND: 60.0/285.0+316.0/5166.0
    final QUSE: 0.0/22.8+37.0/269.0
    RTTFactor: Min: 1.0/1.0+0.5/68.6, Avg: 1.0/1.2+1.5/152.2, Dev: 0.1, Max: 1.0/1.3+4.0/658.8
    init minRTTFactor: 1.0/1.1+1.3/226.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 135139
  Total circs: 135139
  Circs still in SS: 135139

Network statistics for circuits that hit min cwnd:
  No circuits
TLS overhead in /shadow/avg3cmux/16326/48545/tornet/1/heartbeat-nonexit.log min/avg+dev/max: 0.79/1.69+1.4/8.46
TLS overhead in /shadow/avg3cmux/16326/48545/tornet/1/heartbeat-exit.log min/avg+dev/max: 0.73/2.69+4.7/25.31
